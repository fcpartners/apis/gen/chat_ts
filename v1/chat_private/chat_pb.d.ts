// package: fcp.chat.v1.chat_private
// file: v1/chat_private/chat.proto

import * as jspb from "google-protobuf";
import * as v1_chat_common_pb from "../../v1/chat/common_pb";
import * as v1_chat_enum_pb from "../../v1/chat/enum_pb";
import * as v1_chat_model_pb from "../../v1/chat/model_pb";
import * as v1_chat_user_pb from "../../v1/chat/user_pb";

export class Tag extends jspb.Message {
  getObjectType(): ObjectTypeMap[keyof ObjectTypeMap];
  setObjectType(value: ObjectTypeMap[keyof ObjectTypeMap]): void;

  getObjectId(): string;
  setObjectId(value: string): void;

  getCreatedAtUnix(): number;
  setCreatedAtUnix(value: number): void;

  getProductType(): number;
  setProductType(value: number): void;

  getCustomerName(): string;
  setCustomerName(value: string): void;

  getSupplierName(): string;
  setSupplierName(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Tag.AsObject;
  static toObject(includeInstance: boolean, msg: Tag): Tag.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Tag, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Tag;
  static deserializeBinaryFromReader(message: Tag, reader: jspb.BinaryReader): Tag;
}

export namespace Tag {
  export type AsObject = {
    objectType: ObjectTypeMap[keyof ObjectTypeMap],
    objectId: string,
    createdAtUnix: number,
    productType: number,
    customerName: string,
    supplierName: string,
  }
}

export class CreateRequest extends jspb.Message {
  getType(): v1_chat_enum_pb.ChatTypeMap[keyof v1_chat_enum_pb.ChatTypeMap];
  setType(value: v1_chat_enum_pb.ChatTypeMap[keyof v1_chat_enum_pb.ChatTypeMap]): void;

  getCaption(): string;
  setCaption(value: string): void;

  clearUsersList(): void;
  getUsersList(): Array<v1_chat_user_pb.User>;
  setUsersList(value: Array<v1_chat_user_pb.User>): void;
  addUsers(value?: v1_chat_user_pb.User, index?: number): v1_chat_user_pb.User;

  clearMessagesList(): void;
  getMessagesList(): Array<string>;
  setMessagesList(value: Array<string>): void;
  addMessages(value: string, index?: number): string;

  hasTag(): boolean;
  clearTag(): void;
  getTag(): Tag | undefined;
  setTag(value?: Tag): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateRequest): CreateRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateRequest;
  static deserializeBinaryFromReader(message: CreateRequest, reader: jspb.BinaryReader): CreateRequest;
}

export namespace CreateRequest {
  export type AsObject = {
    type: v1_chat_enum_pb.ChatTypeMap[keyof v1_chat_enum_pb.ChatTypeMap],
    caption: string,
    usersList: Array<v1_chat_user_pb.User.AsObject>,
    messagesList: Array<string>,
    tag?: Tag.AsObject,
  }
}

export class AddMessagesRequest extends jspb.Message {
  getTopic(): string;
  setTopic(value: string): void;

  clearMessagesList(): void;
  getMessagesList(): Array<string>;
  setMessagesList(value: Array<string>): void;
  addMessages(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddMessagesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AddMessagesRequest): AddMessagesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AddMessagesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddMessagesRequest;
  static deserializeBinaryFromReader(message: AddMessagesRequest, reader: jspb.BinaryReader): AddMessagesRequest;
}

export namespace AddMessagesRequest {
  export type AsObject = {
    topic: string,
    messagesList: Array<string>,
  }
}

export interface ObjectTypeMap {
  UNSPECIFIED: 0;
  DEAL: 1;
  FAST_DEAL: 2;
}

export const ObjectType: ObjectTypeMap;

