// package: fcp.chat.v1.chat_private
// file: v1/chat_private/chat.proto

import * as v1_chat_private_chat_pb from "../../v1/chat_private/chat_pb";
import * as v1_chat_model_pb from "../../v1/chat/model_pb";
import {grpc} from "@improbable-eng/grpc-web";

type ChatServiceCreate = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_private_chat_pb.CreateRequest;
  readonly responseType: typeof v1_chat_model_pb.ServerComMessage;
};

type ChatServiceAddMessages = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_private_chat_pb.AddMessagesRequest;
  readonly responseType: typeof v1_chat_model_pb.ServerComMessage;
};

export class ChatService {
  static readonly serviceName: string;
  static readonly Create: ChatServiceCreate;
  static readonly AddMessages: ChatServiceAddMessages;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class ChatServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  create(
    requestMessage: v1_chat_private_chat_pb.CreateRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_chat_model_pb.ServerComMessage|null) => void
  ): UnaryResponse;
  create(
    requestMessage: v1_chat_private_chat_pb.CreateRequest,
    callback: (error: ServiceError|null, responseMessage: v1_chat_model_pb.ServerComMessage|null) => void
  ): UnaryResponse;
  addMessages(
    requestMessage: v1_chat_private_chat_pb.AddMessagesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_chat_model_pb.ServerComMessage|null) => void
  ): UnaryResponse;
  addMessages(
    requestMessage: v1_chat_private_chat_pb.AddMessagesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_chat_model_pb.ServerComMessage|null) => void
  ): UnaryResponse;
}

