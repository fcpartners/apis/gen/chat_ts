// package: fcp.chat.v1.chat_private
// file: v1/chat_private/chat.proto

var v1_chat_private_chat_pb = require("../../v1/chat_private/chat_pb");
var v1_chat_model_pb = require("../../v1/chat/model_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var ChatService = (function () {
  function ChatService() {}
  ChatService.serviceName = "fcp.chat.v1.chat_private.ChatService";
  return ChatService;
}());

ChatService.Create = {
  methodName: "Create",
  service: ChatService,
  requestStream: false,
  responseStream: false,
  requestType: v1_chat_private_chat_pb.CreateRequest,
  responseType: v1_chat_model_pb.ServerComMessage
};

ChatService.AddMessages = {
  methodName: "AddMessages",
  service: ChatService,
  requestStream: false,
  responseStream: false,
  requestType: v1_chat_private_chat_pb.AddMessagesRequest,
  responseType: v1_chat_model_pb.ServerComMessage
};

exports.ChatService = ChatService;

function ChatServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

ChatServiceClient.prototype.create = function create(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(ChatService.Create, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

ChatServiceClient.prototype.addMessages = function addMessages(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(ChatService.AddMessages, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.ChatServiceClient = ChatServiceClient;

