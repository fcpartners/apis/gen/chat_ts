// package: fcp.chat.v1.chat
// file: v1/chat/enum.proto

import * as jspb from "google-protobuf";

export interface ChatTypeMap {
  CHAT_UNSPECIFIED: 0;
  CHAT_GROUP: 1;
}

export const ChatType: ChatTypeMap;

