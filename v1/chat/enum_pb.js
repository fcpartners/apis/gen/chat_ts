// source: v1/chat/enum.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

goog.exportSymbol('proto.fcp.chat.v1.chat.ChatType', null, global);
/**
 * @enum {number}
 */
proto.fcp.chat.v1.chat.ChatType = {
  CHAT_UNSPECIFIED: 0,
  CHAT_GROUP: 1
};

goog.object.extend(exports, proto.fcp.chat.v1.chat);
