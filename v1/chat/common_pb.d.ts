// package: fcp.chat.v1.chat
// file: v1/chat/common.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as google_protobuf_duration_pb from "google-protobuf/google/protobuf/duration_pb";

export class Sorting extends jspb.Message {
  getFieldName(): string;
  setFieldName(value: string): void;

  getOrder(): Sorting.OrderMap[keyof Sorting.OrderMap];
  setOrder(value: Sorting.OrderMap[keyof Sorting.OrderMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Sorting.AsObject;
  static toObject(includeInstance: boolean, msg: Sorting): Sorting.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Sorting, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Sorting;
  static deserializeBinaryFromReader(message: Sorting, reader: jspb.BinaryReader): Sorting;
}

export namespace Sorting {
  export type AsObject = {
    fieldName: string,
    order: Sorting.OrderMap[keyof Sorting.OrderMap],
  }

  export interface OrderMap {
    ASC: 0;
    DESC: 1;
  }

  export const Order: OrderMap;
}

export class PaginationRequest extends jspb.Message {
  getPageSize(): number;
  setPageSize(value: number): void;

  getCurrentPage(): number;
  setCurrentPage(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PaginationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: PaginationRequest): PaginationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PaginationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PaginationRequest;
  static deserializeBinaryFromReader(message: PaginationRequest, reader: jspb.BinaryReader): PaginationRequest;
}

export namespace PaginationRequest {
  export type AsObject = {
    pageSize: number,
    currentPage: number,
  }
}

export class PaginationResponse extends jspb.Message {
  getTotalItems(): number;
  setTotalItems(value: number): void;

  getTotalPages(): number;
  setTotalPages(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PaginationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: PaginationResponse): PaginationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PaginationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PaginationResponse;
  static deserializeBinaryFromReader(message: PaginationResponse, reader: jspb.BinaryReader): PaginationResponse;
}

export namespace PaginationResponse {
  export type AsObject = {
    totalItems: number,
    totalPages: number,
  }
}

export class TimeRange extends jspb.Message {
  hasFrom(): boolean;
  clearFrom(): void;
  getFrom(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setFrom(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasTo(): boolean;
  clearTo(): void;
  getTo(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setTo(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TimeRange.AsObject;
  static toObject(includeInstance: boolean, msg: TimeRange): TimeRange.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TimeRange, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TimeRange;
  static deserializeBinaryFromReader(message: TimeRange, reader: jspb.BinaryReader): TimeRange;
}

export namespace TimeRange {
  export type AsObject = {
    from?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    to?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class DurationRange extends jspb.Message {
  hasFrom(): boolean;
  clearFrom(): void;
  getFrom(): google_protobuf_duration_pb.Duration | undefined;
  setFrom(value?: google_protobuf_duration_pb.Duration): void;

  hasTo(): boolean;
  clearTo(): void;
  getTo(): google_protobuf_duration_pb.Duration | undefined;
  setTo(value?: google_protobuf_duration_pb.Duration): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DurationRange.AsObject;
  static toObject(includeInstance: boolean, msg: DurationRange): DurationRange.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DurationRange, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DurationRange;
  static deserializeBinaryFromReader(message: DurationRange, reader: jspb.BinaryReader): DurationRange;
}

export namespace DurationRange {
  export type AsObject = {
    from?: google_protobuf_duration_pb.Duration.AsObject,
    to?: google_protobuf_duration_pb.Duration.AsObject,
  }
}

export class Date extends jspb.Message {
  getYear(): number;
  setYear(value: number): void;

  getMonth(): number;
  setMonth(value: number): void;

  getDay(): number;
  setDay(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Date.AsObject;
  static toObject(includeInstance: boolean, msg: Date): Date.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Date, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Date;
  static deserializeBinaryFromReader(message: Date, reader: jspb.BinaryReader): Date;
}

export namespace Date {
  export type AsObject = {
    year: number,
    month: number,
    day: number,
  }
}

export class DateValue extends jspb.Message {
  hasValue(): boolean;
  clearValue(): void;
  getValue(): Date | undefined;
  setValue(value?: Date): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DateValue.AsObject;
  static toObject(includeInstance: boolean, msg: DateValue): DateValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DateValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DateValue;
  static deserializeBinaryFromReader(message: DateValue, reader: jspb.BinaryReader): DateValue;
}

export namespace DateValue {
  export type AsObject = {
    value?: Date.AsObject,
  }
}

export class ObjectError extends jspb.Message {
  getIndex(): number;
  setIndex(value: number): void;

  getId(): number;
  setId(value: number): void;

  getMsg(): string;
  setMsg(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ObjectError.AsObject;
  static toObject(includeInstance: boolean, msg: ObjectError): ObjectError.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ObjectError, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ObjectError;
  static deserializeBinaryFromReader(message: ObjectError, reader: jspb.BinaryReader): ObjectError;
}

export namespace ObjectError {
  export type AsObject = {
    index: number,
    id: number,
    msg: string,
  }
}

