// package: fcp.chat.v1.chat
// file: v1/chat/model.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";

export class DefaultAcsMode extends jspb.Message {
  getAuth(): string;
  setAuth(value: string): void;

  getAnon(): string;
  setAnon(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DefaultAcsMode.AsObject;
  static toObject(includeInstance: boolean, msg: DefaultAcsMode): DefaultAcsMode.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DefaultAcsMode, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DefaultAcsMode;
  static deserializeBinaryFromReader(message: DefaultAcsMode, reader: jspb.BinaryReader): DefaultAcsMode;
}

export namespace DefaultAcsMode {
  export type AsObject = {
    auth: string,
    anon: string,
  }
}

export class AccessMode extends jspb.Message {
  getWant(): string;
  setWant(value: string): void;

  getGiven(): string;
  setGiven(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AccessMode.AsObject;
  static toObject(includeInstance: boolean, msg: AccessMode): AccessMode.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AccessMode, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AccessMode;
  static deserializeBinaryFromReader(message: AccessMode, reader: jspb.BinaryReader): AccessMode;
}

export namespace AccessMode {
  export type AsObject = {
    want: string,
    given: string,
  }
}

export class SetSub extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getMode(): string;
  setMode(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetSub.AsObject;
  static toObject(includeInstance: boolean, msg: SetSub): SetSub.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetSub, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetSub;
  static deserializeBinaryFromReader(message: SetSub, reader: jspb.BinaryReader): SetSub;
}

export namespace SetSub {
  export type AsObject = {
    userId: string,
    mode: string,
  }
}

export class ClientCred extends jspb.Message {
  getMethod(): string;
  setMethod(value: string): void;

  getValue(): string;
  setValue(value: string): void;

  getResponse(): string;
  setResponse(value: string): void;

  getParamsMap(): jspb.Map<string, Uint8Array | string>;
  clearParamsMap(): void;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientCred.AsObject;
  static toObject(includeInstance: boolean, msg: ClientCred): ClientCred.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientCred, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientCred;
  static deserializeBinaryFromReader(message: ClientCred, reader: jspb.BinaryReader): ClientCred;
}

export namespace ClientCred {
  export type AsObject = {
    method: string,
    value: string,
    response: string,
    paramsMap: Array<[string, Uint8Array | string]>,
  }
}

export class SetDesc extends jspb.Message {
  hasDefaultAcs(): boolean;
  clearDefaultAcs(): void;
  getDefaultAcs(): DefaultAcsMode | undefined;
  setDefaultAcs(value?: DefaultAcsMode): void;

  getPublic(): Uint8Array | string;
  getPublic_asU8(): Uint8Array;
  getPublic_asB64(): string;
  setPublic(value: Uint8Array | string): void;

  getPrivate(): Uint8Array | string;
  getPrivate_asU8(): Uint8Array;
  getPrivate_asB64(): string;
  setPrivate(value: Uint8Array | string): void;

  getTrusted(): Uint8Array | string;
  getTrusted_asU8(): Uint8Array;
  getTrusted_asB64(): string;
  setTrusted(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetDesc.AsObject;
  static toObject(includeInstance: boolean, msg: SetDesc): SetDesc.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetDesc, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetDesc;
  static deserializeBinaryFromReader(message: SetDesc, reader: jspb.BinaryReader): SetDesc;
}

export namespace SetDesc {
  export type AsObject = {
    defaultAcs?: DefaultAcsMode.AsObject,
    public: Uint8Array | string,
    private: Uint8Array | string,
    trusted: Uint8Array | string,
  }
}

export class GetOpts extends jspb.Message {
  getIfModifiedSince(): number;
  setIfModifiedSince(value: number): void;

  getUser(): string;
  setUser(value: string): void;

  getTopic(): string;
  setTopic(value: string): void;

  getSinceId(): number;
  setSinceId(value: number): void;

  getBeforeId(): number;
  setBeforeId(value: number): void;

  getLimit(): number;
  setLimit(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetOpts.AsObject;
  static toObject(includeInstance: boolean, msg: GetOpts): GetOpts.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetOpts, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetOpts;
  static deserializeBinaryFromReader(message: GetOpts, reader: jspb.BinaryReader): GetOpts;
}

export namespace GetOpts {
  export type AsObject = {
    ifModifiedSince: number,
    user: string,
    topic: string,
    sinceId: number,
    beforeId: number,
    limit: number,
  }
}

export class GetQuery extends jspb.Message {
  getWhat(): string;
  setWhat(value: string): void;

  hasDesc(): boolean;
  clearDesc(): void;
  getDesc(): GetOpts | undefined;
  setDesc(value?: GetOpts): void;

  hasSub(): boolean;
  clearSub(): void;
  getSub(): GetOpts | undefined;
  setSub(value?: GetOpts): void;

  hasData(): boolean;
  clearData(): void;
  getData(): GetOpts | undefined;
  setData(value?: GetOpts): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetQuery.AsObject;
  static toObject(includeInstance: boolean, msg: GetQuery): GetQuery.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetQuery, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetQuery;
  static deserializeBinaryFromReader(message: GetQuery, reader: jspb.BinaryReader): GetQuery;
}

export namespace GetQuery {
  export type AsObject = {
    what: string,
    desc?: GetOpts.AsObject,
    sub?: GetOpts.AsObject,
    data?: GetOpts.AsObject,
  }
}

export class SetQuery extends jspb.Message {
  hasDesc(): boolean;
  clearDesc(): void;
  getDesc(): SetDesc | undefined;
  setDesc(value?: SetDesc): void;

  hasSub(): boolean;
  clearSub(): void;
  getSub(): SetSub | undefined;
  setSub(value?: SetSub): void;

  clearTagsList(): void;
  getTagsList(): Array<string>;
  setTagsList(value: Array<string>): void;
  addTags(value: string, index?: number): string;

  hasCred(): boolean;
  clearCred(): void;
  getCred(): ClientCred | undefined;
  setCred(value?: ClientCred): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetQuery.AsObject;
  static toObject(includeInstance: boolean, msg: SetQuery): SetQuery.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetQuery, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetQuery;
  static deserializeBinaryFromReader(message: SetQuery, reader: jspb.BinaryReader): SetQuery;
}

export namespace SetQuery {
  export type AsObject = {
    desc?: SetDesc.AsObject,
    sub?: SetSub.AsObject,
    tagsList: Array<string>,
    cred?: ClientCred.AsObject,
  }
}

export class SeqRange extends jspb.Message {
  getLow(): number;
  setLow(value: number): void;

  getHi(): number;
  setHi(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SeqRange.AsObject;
  static toObject(includeInstance: boolean, msg: SeqRange): SeqRange.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SeqRange, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SeqRange;
  static deserializeBinaryFromReader(message: SeqRange, reader: jspb.BinaryReader): SeqRange;
}

export namespace SeqRange {
  export type AsObject = {
    low: number,
    hi: number,
  }
}

export class ClientHi extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getUserAgent(): string;
  setUserAgent(value: string): void;

  getVer(): string;
  setVer(value: string): void;

  getDeviceId(): string;
  setDeviceId(value: string): void;

  getLang(): string;
  setLang(value: string): void;

  getPlatform(): string;
  setPlatform(value: string): void;

  getBackground(): boolean;
  setBackground(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientHi.AsObject;
  static toObject(includeInstance: boolean, msg: ClientHi): ClientHi.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientHi, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientHi;
  static deserializeBinaryFromReader(message: ClientHi, reader: jspb.BinaryReader): ClientHi;
}

export namespace ClientHi {
  export type AsObject = {
    id: string,
    userAgent: string,
    ver: string,
    deviceId: string,
    lang: string,
    platform: string,
    background: boolean,
  }
}

export class ClientAcc extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getUserId(): string;
  setUserId(value: string): void;

  getScheme(): string;
  setScheme(value: string): void;

  getSecret(): Uint8Array | string;
  getSecret_asU8(): Uint8Array;
  getSecret_asB64(): string;
  setSecret(value: Uint8Array | string): void;

  getLogin(): boolean;
  setLogin(value: boolean): void;

  clearTagsList(): void;
  getTagsList(): Array<string>;
  setTagsList(value: Array<string>): void;
  addTags(value: string, index?: number): string;

  hasDesc(): boolean;
  clearDesc(): void;
  getDesc(): SetDesc | undefined;
  setDesc(value?: SetDesc): void;

  clearCredList(): void;
  getCredList(): Array<ClientCred>;
  setCredList(value: Array<ClientCred>): void;
  addCred(value?: ClientCred, index?: number): ClientCred;

  getToken(): Uint8Array | string;
  getToken_asU8(): Uint8Array;
  getToken_asB64(): string;
  setToken(value: Uint8Array | string): void;

  getState(): string;
  setState(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientAcc.AsObject;
  static toObject(includeInstance: boolean, msg: ClientAcc): ClientAcc.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientAcc, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientAcc;
  static deserializeBinaryFromReader(message: ClientAcc, reader: jspb.BinaryReader): ClientAcc;
}

export namespace ClientAcc {
  export type AsObject = {
    id: string,
    userId: string,
    scheme: string,
    secret: Uint8Array | string,
    login: boolean,
    tagsList: Array<string>,
    desc?: SetDesc.AsObject,
    credList: Array<ClientCred.AsObject>,
    token: Uint8Array | string,
    state: string,
  }
}

export class ClientLogin extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getScheme(): string;
  setScheme(value: string): void;

  getSecret(): Uint8Array | string;
  getSecret_asU8(): Uint8Array;
  getSecret_asB64(): string;
  setSecret(value: Uint8Array | string): void;

  clearCredList(): void;
  getCredList(): Array<ClientCred>;
  setCredList(value: Array<ClientCred>): void;
  addCred(value?: ClientCred, index?: number): ClientCred;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientLogin.AsObject;
  static toObject(includeInstance: boolean, msg: ClientLogin): ClientLogin.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientLogin, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientLogin;
  static deserializeBinaryFromReader(message: ClientLogin, reader: jspb.BinaryReader): ClientLogin;
}

export namespace ClientLogin {
  export type AsObject = {
    id: string,
    scheme: string,
    secret: Uint8Array | string,
    credList: Array<ClientCred.AsObject>,
  }
}

export class ClientSub extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getTopic(): string;
  setTopic(value: string): void;

  hasSetQuery(): boolean;
  clearSetQuery(): void;
  getSetQuery(): SetQuery | undefined;
  setSetQuery(value?: SetQuery): void;

  hasGetQuery(): boolean;
  clearGetQuery(): void;
  getGetQuery(): GetQuery | undefined;
  setGetQuery(value?: GetQuery): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientSub.AsObject;
  static toObject(includeInstance: boolean, msg: ClientSub): ClientSub.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientSub, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientSub;
  static deserializeBinaryFromReader(message: ClientSub, reader: jspb.BinaryReader): ClientSub;
}

export namespace ClientSub {
  export type AsObject = {
    id: string,
    topic: string,
    setQuery?: SetQuery.AsObject,
    getQuery?: GetQuery.AsObject,
  }
}

export class ClientLeave extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getTopic(): string;
  setTopic(value: string): void;

  getUnsub(): boolean;
  setUnsub(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientLeave.AsObject;
  static toObject(includeInstance: boolean, msg: ClientLeave): ClientLeave.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientLeave, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientLeave;
  static deserializeBinaryFromReader(message: ClientLeave, reader: jspb.BinaryReader): ClientLeave;
}

export namespace ClientLeave {
  export type AsObject = {
    id: string,
    topic: string,
    unsub: boolean,
  }
}

export class ClientPub extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getTopic(): string;
  setTopic(value: string): void;

  getNoEcho(): boolean;
  setNoEcho(value: boolean): void;

  getHeadMap(): jspb.Map<string, Uint8Array | string>;
  clearHeadMap(): void;
  getContent(): Uint8Array | string;
  getContent_asU8(): Uint8Array;
  getContent_asB64(): string;
  setContent(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientPub.AsObject;
  static toObject(includeInstance: boolean, msg: ClientPub): ClientPub.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientPub, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientPub;
  static deserializeBinaryFromReader(message: ClientPub, reader: jspb.BinaryReader): ClientPub;
}

export namespace ClientPub {
  export type AsObject = {
    id: string,
    topic: string,
    noEcho: boolean,
    headMap: Array<[string, Uint8Array | string]>,
    content: Uint8Array | string,
  }
}

export class ClientGet extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getTopic(): string;
  setTopic(value: string): void;

  hasQuery(): boolean;
  clearQuery(): void;
  getQuery(): GetQuery | undefined;
  setQuery(value?: GetQuery): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientGet.AsObject;
  static toObject(includeInstance: boolean, msg: ClientGet): ClientGet.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientGet, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientGet;
  static deserializeBinaryFromReader(message: ClientGet, reader: jspb.BinaryReader): ClientGet;
}

export namespace ClientGet {
  export type AsObject = {
    id: string,
    topic: string,
    query?: GetQuery.AsObject,
  }
}

export class ClientSet extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getTopic(): string;
  setTopic(value: string): void;

  hasQuery(): boolean;
  clearQuery(): void;
  getQuery(): SetQuery | undefined;
  setQuery(value?: SetQuery): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientSet.AsObject;
  static toObject(includeInstance: boolean, msg: ClientSet): ClientSet.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientSet, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientSet;
  static deserializeBinaryFromReader(message: ClientSet, reader: jspb.BinaryReader): ClientSet;
}

export namespace ClientSet {
  export type AsObject = {
    id: string,
    topic: string,
    query?: SetQuery.AsObject,
  }
}

export class ClientDel extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getTopic(): string;
  setTopic(value: string): void;

  getWhat(): ClientDel.WhatMap[keyof ClientDel.WhatMap];
  setWhat(value: ClientDel.WhatMap[keyof ClientDel.WhatMap]): void;

  clearDelSeqList(): void;
  getDelSeqList(): Array<SeqRange>;
  setDelSeqList(value: Array<SeqRange>): void;
  addDelSeq(value?: SeqRange, index?: number): SeqRange;

  getUserId(): string;
  setUserId(value: string): void;

  hasCred(): boolean;
  clearCred(): void;
  getCred(): ClientCred | undefined;
  setCred(value?: ClientCred): void;

  getHard(): boolean;
  setHard(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientDel.AsObject;
  static toObject(includeInstance: boolean, msg: ClientDel): ClientDel.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientDel, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientDel;
  static deserializeBinaryFromReader(message: ClientDel, reader: jspb.BinaryReader): ClientDel;
}

export namespace ClientDel {
  export type AsObject = {
    id: string,
    topic: string,
    what: ClientDel.WhatMap[keyof ClientDel.WhatMap],
    delSeqList: Array<SeqRange.AsObject>,
    userId: string,
    cred?: ClientCred.AsObject,
    hard: boolean,
  }

  export interface WhatMap {
    X0: 0;
    MSG: 1;
    TOPIC: 2;
    SUB: 3;
    USER: 4;
    CRED: 5;
  }

  export const What: WhatMap;
}

export class ClientNote extends jspb.Message {
  getTopic(): string;
  setTopic(value: string): void;

  getWhat(): InfoNoteMap[keyof InfoNoteMap];
  setWhat(value: InfoNoteMap[keyof InfoNoteMap]): void;

  getSeqId(): number;
  setSeqId(value: number): void;

  getUnread(): number;
  setUnread(value: number): void;

  getEvent(): CallEventMap[keyof CallEventMap];
  setEvent(value: CallEventMap[keyof CallEventMap]): void;

  getPayload(): Uint8Array | string;
  getPayload_asU8(): Uint8Array;
  getPayload_asB64(): string;
  setPayload(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientNote.AsObject;
  static toObject(includeInstance: boolean, msg: ClientNote): ClientNote.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientNote, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientNote;
  static deserializeBinaryFromReader(message: ClientNote, reader: jspb.BinaryReader): ClientNote;
}

export namespace ClientNote {
  export type AsObject = {
    topic: string,
    what: InfoNoteMap[keyof InfoNoteMap],
    seqId: number,
    unread: number,
    event: CallEventMap[keyof CallEventMap],
    payload: Uint8Array | string,
  }
}

export class ClientExtra extends jspb.Message {
  clearAttachmentsList(): void;
  getAttachmentsList(): Array<string>;
  setAttachmentsList(value: Array<string>): void;
  addAttachments(value: string, index?: number): string;

  getOnBehalfOf(): string;
  setOnBehalfOf(value: string): void;

  getAuthLevel(): AuthLevelMap[keyof AuthLevelMap];
  setAuthLevel(value: AuthLevelMap[keyof AuthLevelMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientExtra.AsObject;
  static toObject(includeInstance: boolean, msg: ClientExtra): ClientExtra.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientExtra, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientExtra;
  static deserializeBinaryFromReader(message: ClientExtra, reader: jspb.BinaryReader): ClientExtra;
}

export namespace ClientExtra {
  export type AsObject = {
    attachmentsList: Array<string>,
    onBehalfOf: string,
    authLevel: AuthLevelMap[keyof AuthLevelMap],
  }
}

export class ClientMsg extends jspb.Message {
  hasHi(): boolean;
  clearHi(): void;
  getHi(): ClientHi | undefined;
  setHi(value?: ClientHi): void;

  hasAcc(): boolean;
  clearAcc(): void;
  getAcc(): ClientAcc | undefined;
  setAcc(value?: ClientAcc): void;

  hasLogin(): boolean;
  clearLogin(): void;
  getLogin(): ClientLogin | undefined;
  setLogin(value?: ClientLogin): void;

  hasSub(): boolean;
  clearSub(): void;
  getSub(): ClientSub | undefined;
  setSub(value?: ClientSub): void;

  hasLeave(): boolean;
  clearLeave(): void;
  getLeave(): ClientLeave | undefined;
  setLeave(value?: ClientLeave): void;

  hasPub(): boolean;
  clearPub(): void;
  getPub(): ClientPub | undefined;
  setPub(value?: ClientPub): void;

  hasGet(): boolean;
  clearGet(): void;
  getGet(): ClientGet | undefined;
  setGet(value?: ClientGet): void;

  hasSet(): boolean;
  clearSet(): void;
  getSet(): ClientSet | undefined;
  setSet(value?: ClientSet): void;

  hasDel(): boolean;
  clearDel(): void;
  getDel(): ClientDel | undefined;
  setDel(value?: ClientDel): void;

  hasNote(): boolean;
  clearNote(): void;
  getNote(): ClientNote | undefined;
  setNote(value?: ClientNote): void;

  hasExtra(): boolean;
  clearExtra(): void;
  getExtra(): ClientExtra | undefined;
  setExtra(value?: ClientExtra): void;

  getMessageCase(): ClientMsg.MessageCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientMsg.AsObject;
  static toObject(includeInstance: boolean, msg: ClientMsg): ClientMsg.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientMsg, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientMsg;
  static deserializeBinaryFromReader(message: ClientMsg, reader: jspb.BinaryReader): ClientMsg;
}

export namespace ClientMsg {
  export type AsObject = {
    hi?: ClientHi.AsObject,
    acc?: ClientAcc.AsObject,
    login?: ClientLogin.AsObject,
    sub?: ClientSub.AsObject,
    leave?: ClientLeave.AsObject,
    pub?: ClientPub.AsObject,
    get?: ClientGet.AsObject,
    set?: ClientSet.AsObject,
    del?: ClientDel.AsObject,
    note?: ClientNote.AsObject,
    extra?: ClientExtra.AsObject,
  }

  export enum MessageCase {
    MESSAGE_NOT_SET = 0,
    HI = 1,
    ACC = 2,
    LOGIN = 3,
    SUB = 4,
    LEAVE = 5,
    PUB = 6,
    GET = 7,
    SET = 8,
    DEL = 9,
    NOTE = 10,
  }
}

export class ServerCred extends jspb.Message {
  getMethod(): string;
  setMethod(value: string): void;

  getValue(): string;
  setValue(value: string): void;

  getDone(): boolean;
  setDone(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ServerCred.AsObject;
  static toObject(includeInstance: boolean, msg: ServerCred): ServerCred.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ServerCred, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ServerCred;
  static deserializeBinaryFromReader(message: ServerCred, reader: jspb.BinaryReader): ServerCred;
}

export namespace ServerCred {
  export type AsObject = {
    method: string,
    value: string,
    done: boolean,
  }
}

export class TopicDesc extends jspb.Message {
  getCreatedAt(): number;
  setCreatedAt(value: number): void;

  getUpdatedAt(): number;
  setUpdatedAt(value: number): void;

  getTouchedAt(): number;
  setTouchedAt(value: number): void;

  hasDefacs(): boolean;
  clearDefacs(): void;
  getDefacs(): DefaultAcsMode | undefined;
  setDefacs(value?: DefaultAcsMode): void;

  hasAcs(): boolean;
  clearAcs(): void;
  getAcs(): AccessMode | undefined;
  setAcs(value?: AccessMode): void;

  getSeqId(): number;
  setSeqId(value: number): void;

  getReadId(): number;
  setReadId(value: number): void;

  getRecvId(): number;
  setRecvId(value: number): void;

  getDelId(): number;
  setDelId(value: number): void;

  getPublic(): Uint8Array | string;
  getPublic_asU8(): Uint8Array;
  getPublic_asB64(): string;
  setPublic(value: Uint8Array | string): void;

  getPrivate(): Uint8Array | string;
  getPrivate_asU8(): Uint8Array;
  getPrivate_asB64(): string;
  setPrivate(value: Uint8Array | string): void;

  getState(): string;
  setState(value: string): void;

  getStateAt(): number;
  setStateAt(value: number): void;

  getTrusted(): Uint8Array | string;
  getTrusted_asU8(): Uint8Array;
  getTrusted_asB64(): string;
  setTrusted(value: Uint8Array | string): void;

  getIsChan(): boolean;
  setIsChan(value: boolean): void;

  getLastSeenTime(): number;
  setLastSeenTime(value: number): void;

  getLastSeenUserAgent(): string;
  setLastSeenUserAgent(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TopicDesc.AsObject;
  static toObject(includeInstance: boolean, msg: TopicDesc): TopicDesc.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TopicDesc, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TopicDesc;
  static deserializeBinaryFromReader(message: TopicDesc, reader: jspb.BinaryReader): TopicDesc;
}

export namespace TopicDesc {
  export type AsObject = {
    createdAt: number,
    updatedAt: number,
    touchedAt: number,
    defacs?: DefaultAcsMode.AsObject,
    acs?: AccessMode.AsObject,
    seqId: number,
    readId: number,
    recvId: number,
    delId: number,
    public: Uint8Array | string,
    private: Uint8Array | string,
    state: string,
    stateAt: number,
    trusted: Uint8Array | string,
    isChan: boolean,
    lastSeenTime: number,
    lastSeenUserAgent: string,
  }
}

export class TopicSub extends jspb.Message {
  getUpdatedAt(): number;
  setUpdatedAt(value: number): void;

  getDeletedAt(): number;
  setDeletedAt(value: number): void;

  getOnline(): boolean;
  setOnline(value: boolean): void;

  hasAcs(): boolean;
  clearAcs(): void;
  getAcs(): AccessMode | undefined;
  setAcs(value?: AccessMode): void;

  getReadId(): number;
  setReadId(value: number): void;

  getRecvId(): number;
  setRecvId(value: number): void;

  getPublic(): Uint8Array | string;
  getPublic_asU8(): Uint8Array;
  getPublic_asB64(): string;
  setPublic(value: Uint8Array | string): void;

  getTrusted(): Uint8Array | string;
  getTrusted_asU8(): Uint8Array;
  getTrusted_asB64(): string;
  setTrusted(value: Uint8Array | string): void;

  getPrivate(): Uint8Array | string;
  getPrivate_asU8(): Uint8Array;
  getPrivate_asB64(): string;
  setPrivate(value: Uint8Array | string): void;

  getUserId(): string;
  setUserId(value: string): void;

  getTopic(): string;
  setTopic(value: string): void;

  getTouchedAt(): number;
  setTouchedAt(value: number): void;

  getSeqId(): number;
  setSeqId(value: number): void;

  getDelId(): number;
  setDelId(value: number): void;

  getLastSeenTime(): number;
  setLastSeenTime(value: number): void;

  getLastSeenUserAgent(): string;
  setLastSeenUserAgent(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TopicSub.AsObject;
  static toObject(includeInstance: boolean, msg: TopicSub): TopicSub.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TopicSub, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TopicSub;
  static deserializeBinaryFromReader(message: TopicSub, reader: jspb.BinaryReader): TopicSub;
}

export namespace TopicSub {
  export type AsObject = {
    updatedAt: number,
    deletedAt: number,
    online: boolean,
    acs?: AccessMode.AsObject,
    readId: number,
    recvId: number,
    public: Uint8Array | string,
    trusted: Uint8Array | string,
    private: Uint8Array | string,
    userId: string,
    topic: string,
    touchedAt: number,
    seqId: number,
    delId: number,
    lastSeenTime: number,
    lastSeenUserAgent: string,
  }
}

export class DelValues extends jspb.Message {
  getDelId(): number;
  setDelId(value: number): void;

  clearDelSeqList(): void;
  getDelSeqList(): Array<SeqRange>;
  setDelSeqList(value: Array<SeqRange>): void;
  addDelSeq(value?: SeqRange, index?: number): SeqRange;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DelValues.AsObject;
  static toObject(includeInstance: boolean, msg: DelValues): DelValues.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DelValues, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DelValues;
  static deserializeBinaryFromReader(message: DelValues, reader: jspb.BinaryReader): DelValues;
}

export namespace DelValues {
  export type AsObject = {
    delId: number,
    delSeqList: Array<SeqRange.AsObject>,
  }
}

export class ServerCtrl extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getTopic(): string;
  setTopic(value: string): void;

  getCode(): number;
  setCode(value: number): void;

  getText(): string;
  setText(value: string): void;

  getParamsMap(): jspb.Map<string, Uint8Array | string>;
  clearParamsMap(): void;
  hasTs(): boolean;
  clearTs(): void;
  getTs(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setTs(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ServerCtrl.AsObject;
  static toObject(includeInstance: boolean, msg: ServerCtrl): ServerCtrl.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ServerCtrl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ServerCtrl;
  static deserializeBinaryFromReader(message: ServerCtrl, reader: jspb.BinaryReader): ServerCtrl;
}

export namespace ServerCtrl {
  export type AsObject = {
    id: string,
    topic: string,
    code: number,
    text: string,
    paramsMap: Array<[string, Uint8Array | string]>,
    ts?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class ServerData extends jspb.Message {
  getTopic(): string;
  setTopic(value: string): void;

  getFromUserId(): string;
  setFromUserId(value: string): void;

  getTimestamp(): number;
  setTimestamp(value: number): void;

  getDeletedAt(): number;
  setDeletedAt(value: number): void;

  getSeqId(): number;
  setSeqId(value: number): void;

  getHeadMap(): jspb.Map<string, Uint8Array | string>;
  clearHeadMap(): void;
  getContent(): Uint8Array | string;
  getContent_asU8(): Uint8Array;
  getContent_asB64(): string;
  setContent(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ServerData.AsObject;
  static toObject(includeInstance: boolean, msg: ServerData): ServerData.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ServerData, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ServerData;
  static deserializeBinaryFromReader(message: ServerData, reader: jspb.BinaryReader): ServerData;
}

export namespace ServerData {
  export type AsObject = {
    topic: string,
    fromUserId: string,
    timestamp: number,
    deletedAt: number,
    seqId: number,
    headMap: Array<[string, Uint8Array | string]>,
    content: Uint8Array | string,
  }
}

export class ServerPres extends jspb.Message {
  getTopic(): string;
  setTopic(value: string): void;

  getSrc(): string;
  setSrc(value: string): void;

  getWhat(): ServerPres.WhatMap[keyof ServerPres.WhatMap];
  setWhat(value: ServerPres.WhatMap[keyof ServerPres.WhatMap]): void;

  getUserAgent(): string;
  setUserAgent(value: string): void;

  getSeqId(): number;
  setSeqId(value: number): void;

  getDelId(): number;
  setDelId(value: number): void;

  clearDelSeqList(): void;
  getDelSeqList(): Array<SeqRange>;
  setDelSeqList(value: Array<SeqRange>): void;
  addDelSeq(value?: SeqRange, index?: number): SeqRange;

  getTargetUserId(): string;
  setTargetUserId(value: string): void;

  getActorUserId(): string;
  setActorUserId(value: string): void;

  hasAcs(): boolean;
  clearAcs(): void;
  getAcs(): AccessMode | undefined;
  setAcs(value?: AccessMode): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ServerPres.AsObject;
  static toObject(includeInstance: boolean, msg: ServerPres): ServerPres.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ServerPres, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ServerPres;
  static deserializeBinaryFromReader(message: ServerPres, reader: jspb.BinaryReader): ServerPres;
}

export namespace ServerPres {
  export type AsObject = {
    topic: string,
    src: string,
    what: ServerPres.WhatMap[keyof ServerPres.WhatMap],
    userAgent: string,
    seqId: number,
    delId: number,
    delSeqList: Array<SeqRange.AsObject>,
    targetUserId: string,
    actorUserId: string,
    acs?: AccessMode.AsObject,
  }

  export interface WhatMap {
    X3: 0;
    ON: 1;
    OFF: 2;
    UA: 3;
    UPD: 4;
    GONE: 5;
    ACS: 6;
    TERM: 7;
    MSG: 8;
    READ: 9;
    RECV: 10;
    DEL: 11;
    TAGS: 12;
  }

  export const What: WhatMap;
}

export class ServerMeta extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getTopic(): string;
  setTopic(value: string): void;

  hasDesc(): boolean;
  clearDesc(): void;
  getDesc(): TopicDesc | undefined;
  setDesc(value?: TopicDesc): void;

  clearSubList(): void;
  getSubList(): Array<TopicSub>;
  setSubList(value: Array<TopicSub>): void;
  addSub(value?: TopicSub, index?: number): TopicSub;

  hasDel(): boolean;
  clearDel(): void;
  getDel(): DelValues | undefined;
  setDel(value?: DelValues): void;

  clearTagsList(): void;
  getTagsList(): Array<string>;
  setTagsList(value: Array<string>): void;
  addTags(value: string, index?: number): string;

  clearCredList(): void;
  getCredList(): Array<ServerCred>;
  setCredList(value: Array<ServerCred>): void;
  addCred(value?: ServerCred, index?: number): ServerCred;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ServerMeta.AsObject;
  static toObject(includeInstance: boolean, msg: ServerMeta): ServerMeta.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ServerMeta, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ServerMeta;
  static deserializeBinaryFromReader(message: ServerMeta, reader: jspb.BinaryReader): ServerMeta;
}

export namespace ServerMeta {
  export type AsObject = {
    id: string,
    topic: string,
    desc?: TopicDesc.AsObject,
    subList: Array<TopicSub.AsObject>,
    del?: DelValues.AsObject,
    tagsList: Array<string>,
    credList: Array<ServerCred.AsObject>,
  }
}

export class ServerInfo extends jspb.Message {
  getTopic(): string;
  setTopic(value: string): void;

  getFromUserId(): string;
  setFromUserId(value: string): void;

  getWhat(): InfoNoteMap[keyof InfoNoteMap];
  setWhat(value: InfoNoteMap[keyof InfoNoteMap]): void;

  getSeqId(): number;
  setSeqId(value: number): void;

  getSrc(): string;
  setSrc(value: string): void;

  getEvent(): CallEventMap[keyof CallEventMap];
  setEvent(value: CallEventMap[keyof CallEventMap]): void;

  getPayload(): Uint8Array | string;
  getPayload_asU8(): Uint8Array;
  getPayload_asB64(): string;
  setPayload(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ServerInfo.AsObject;
  static toObject(includeInstance: boolean, msg: ServerInfo): ServerInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ServerInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ServerInfo;
  static deserializeBinaryFromReader(message: ServerInfo, reader: jspb.BinaryReader): ServerInfo;
}

export namespace ServerInfo {
  export type AsObject = {
    topic: string,
    fromUserId: string,
    what: InfoNoteMap[keyof InfoNoteMap],
    seqId: number,
    src: string,
    event: CallEventMap[keyof CallEventMap],
    payload: Uint8Array | string,
  }
}

export class ServerComMessage extends jspb.Message {
  hasCtrl(): boolean;
  clearCtrl(): void;
  getCtrl(): ServerCtrl | undefined;
  setCtrl(value?: ServerCtrl): void;

  hasData(): boolean;
  clearData(): void;
  getData(): ServerData | undefined;
  setData(value?: ServerData): void;

  hasPres(): boolean;
  clearPres(): void;
  getPres(): ServerPres | undefined;
  setPres(value?: ServerPres): void;

  hasMeta(): boolean;
  clearMeta(): void;
  getMeta(): ServerMeta | undefined;
  setMeta(value?: ServerMeta): void;

  hasInfo(): boolean;
  clearInfo(): void;
  getInfo(): ServerInfo | undefined;
  setInfo(value?: ServerInfo): void;

  getTopic(): string;
  setTopic(value: string): void;

  getMessageCase(): ServerComMessage.MessageCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ServerComMessage.AsObject;
  static toObject(includeInstance: boolean, msg: ServerComMessage): ServerComMessage.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ServerComMessage, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ServerComMessage;
  static deserializeBinaryFromReader(message: ServerComMessage, reader: jspb.BinaryReader): ServerComMessage;
}

export namespace ServerComMessage {
  export type AsObject = {
    ctrl?: ServerCtrl.AsObject,
    data?: ServerData.AsObject,
    pres?: ServerPres.AsObject,
    meta?: ServerMeta.AsObject,
    info?: ServerInfo.AsObject,
    topic: string,
  }

  export enum MessageCase {
    MESSAGE_NOT_SET = 0,
    CTRL = 1,
    DATA = 2,
    PRES = 3,
    META = 4,
    INFO = 5,
  }
}

export class ServerResp extends jspb.Message {
  getStatus(): RespCodeMap[keyof RespCodeMap];
  setStatus(value: RespCodeMap[keyof RespCodeMap]): void;

  hasSrvmsg(): boolean;
  clearSrvmsg(): void;
  getSrvmsg(): ServerComMessage | undefined;
  setSrvmsg(value?: ServerComMessage): void;

  hasClmsg(): boolean;
  clearClmsg(): void;
  getClmsg(): ClientMsg | undefined;
  setClmsg(value?: ClientMsg): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ServerResp.AsObject;
  static toObject(includeInstance: boolean, msg: ServerResp): ServerResp.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ServerResp, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ServerResp;
  static deserializeBinaryFromReader(message: ServerResp, reader: jspb.BinaryReader): ServerResp;
}

export namespace ServerResp {
  export type AsObject = {
    status: RespCodeMap[keyof RespCodeMap],
    srvmsg?: ServerComMessage.AsObject,
    clmsg?: ClientMsg.AsObject,
  }
}

export class Session extends jspb.Message {
  getSessionId(): string;
  setSessionId(value: string): void;

  getUserId(): string;
  setUserId(value: string): void;

  getAuthLevel(): AuthLevelMap[keyof AuthLevelMap];
  setAuthLevel(value: AuthLevelMap[keyof AuthLevelMap]): void;

  getRemoteAddr(): string;
  setRemoteAddr(value: string): void;

  getUserAgent(): string;
  setUserAgent(value: string): void;

  getDeviceId(): string;
  setDeviceId(value: string): void;

  getLanguage(): string;
  setLanguage(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Session.AsObject;
  static toObject(includeInstance: boolean, msg: Session): Session.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Session, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Session;
  static deserializeBinaryFromReader(message: Session, reader: jspb.BinaryReader): Session;
}

export namespace Session {
  export type AsObject = {
    sessionId: string,
    userId: string,
    authLevel: AuthLevelMap[keyof AuthLevelMap],
    remoteAddr: string,
    userAgent: string,
    deviceId: string,
    language: string,
  }
}

export class ClientReq extends jspb.Message {
  hasMsg(): boolean;
  clearMsg(): void;
  getMsg(): ClientMsg | undefined;
  setMsg(value?: ClientMsg): void;

  hasSess(): boolean;
  clearSess(): void;
  getSess(): Session | undefined;
  setSess(value?: Session): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientReq.AsObject;
  static toObject(includeInstance: boolean, msg: ClientReq): ClientReq.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ClientReq, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientReq;
  static deserializeBinaryFromReader(message: ClientReq, reader: jspb.BinaryReader): ClientReq;
}

export namespace ClientReq {
  export type AsObject = {
    msg?: ClientMsg.AsObject,
    sess?: Session.AsObject,
  }
}

export class SearchQuery extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getQuery(): string;
  setQuery(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchQuery.AsObject;
  static toObject(includeInstance: boolean, msg: SearchQuery): SearchQuery.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchQuery, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchQuery;
  static deserializeBinaryFromReader(message: SearchQuery, reader: jspb.BinaryReader): SearchQuery;
}

export namespace SearchQuery {
  export type AsObject = {
    userId: string,
    query: string,
  }
}

export class SearchFound extends jspb.Message {
  getStatus(): RespCodeMap[keyof RespCodeMap];
  setStatus(value: RespCodeMap[keyof RespCodeMap]): void;

  getQuery(): string;
  setQuery(value: string): void;

  clearResultList(): void;
  getResultList(): Array<TopicSub>;
  setResultList(value: Array<TopicSub>): void;
  addResult(value?: TopicSub, index?: number): TopicSub;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchFound.AsObject;
  static toObject(includeInstance: boolean, msg: SearchFound): SearchFound.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchFound, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchFound;
  static deserializeBinaryFromReader(message: SearchFound, reader: jspb.BinaryReader): SearchFound;
}

export namespace SearchFound {
  export type AsObject = {
    status: RespCodeMap[keyof RespCodeMap],
    query: string,
    resultList: Array<TopicSub.AsObject>,
  }
}

export class TopicEvent extends jspb.Message {
  getAction(): CrudMap[keyof CrudMap];
  setAction(value: CrudMap[keyof CrudMap]): void;

  getName(): string;
  setName(value: string): void;

  hasDesc(): boolean;
  clearDesc(): void;
  getDesc(): TopicDesc | undefined;
  setDesc(value?: TopicDesc): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TopicEvent.AsObject;
  static toObject(includeInstance: boolean, msg: TopicEvent): TopicEvent.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TopicEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TopicEvent;
  static deserializeBinaryFromReader(message: TopicEvent, reader: jspb.BinaryReader): TopicEvent;
}

export namespace TopicEvent {
  export type AsObject = {
    action: CrudMap[keyof CrudMap],
    name: string,
    desc?: TopicDesc.AsObject,
  }
}

export class AccountEvent extends jspb.Message {
  getAction(): CrudMap[keyof CrudMap];
  setAction(value: CrudMap[keyof CrudMap]): void;

  getUserId(): string;
  setUserId(value: string): void;

  hasDefaultAcs(): boolean;
  clearDefaultAcs(): void;
  getDefaultAcs(): DefaultAcsMode | undefined;
  setDefaultAcs(value?: DefaultAcsMode): void;

  getPublic(): Uint8Array | string;
  getPublic_asU8(): Uint8Array;
  getPublic_asB64(): string;
  setPublic(value: Uint8Array | string): void;

  clearTagsList(): void;
  getTagsList(): Array<string>;
  setTagsList(value: Array<string>): void;
  addTags(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AccountEvent.AsObject;
  static toObject(includeInstance: boolean, msg: AccountEvent): AccountEvent.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AccountEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AccountEvent;
  static deserializeBinaryFromReader(message: AccountEvent, reader: jspb.BinaryReader): AccountEvent;
}

export namespace AccountEvent {
  export type AsObject = {
    action: CrudMap[keyof CrudMap],
    userId: string,
    defaultAcs?: DefaultAcsMode.AsObject,
    public: Uint8Array | string,
    tagsList: Array<string>,
  }
}

export class SubscriptionEvent extends jspb.Message {
  getAction(): CrudMap[keyof CrudMap];
  setAction(value: CrudMap[keyof CrudMap]): void;

  getTopic(): string;
  setTopic(value: string): void;

  getUserId(): string;
  setUserId(value: string): void;

  getDelId(): number;
  setDelId(value: number): void;

  getReadId(): number;
  setReadId(value: number): void;

  getRecvId(): number;
  setRecvId(value: number): void;

  hasMode(): boolean;
  clearMode(): void;
  getMode(): AccessMode | undefined;
  setMode(value?: AccessMode): void;

  getPrivate(): Uint8Array | string;
  getPrivate_asU8(): Uint8Array;
  getPrivate_asB64(): string;
  setPrivate(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SubscriptionEvent.AsObject;
  static toObject(includeInstance: boolean, msg: SubscriptionEvent): SubscriptionEvent.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SubscriptionEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SubscriptionEvent;
  static deserializeBinaryFromReader(message: SubscriptionEvent, reader: jspb.BinaryReader): SubscriptionEvent;
}

export namespace SubscriptionEvent {
  export type AsObject = {
    action: CrudMap[keyof CrudMap],
    topic: string,
    userId: string,
    delId: number,
    readId: number,
    recvId: number,
    mode?: AccessMode.AsObject,
    private: Uint8Array | string,
  }
}

export class MessageEvent extends jspb.Message {
  getAction(): CrudMap[keyof CrudMap];
  setAction(value: CrudMap[keyof CrudMap]): void;

  hasMsg(): boolean;
  clearMsg(): void;
  getMsg(): ServerData | undefined;
  setMsg(value?: ServerData): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MessageEvent.AsObject;
  static toObject(includeInstance: boolean, msg: MessageEvent): MessageEvent.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MessageEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MessageEvent;
  static deserializeBinaryFromReader(message: MessageEvent, reader: jspb.BinaryReader): MessageEvent;
}

export namespace MessageEvent {
  export type AsObject = {
    action: CrudMap[keyof CrudMap],
    msg?: ServerData.AsObject,
  }
}

export interface AuthLevelMap {
  NONE: 0;
  ANON: 10;
  AUTH: 20;
  ROOT: 30;
}

export const AuthLevel: AuthLevelMap;

export interface InfoNoteMap {
  X1: 0;
  READ: 1;
  RECV: 2;
  KP: 3;
  CALL: 4;
}

export const InfoNote: InfoNoteMap;

export interface CallEventMap {
  X2: 0;
  ACCEPT: 1;
  ANSWER: 2;
  HANG_UP: 3;
  ICE_CANDIDATE: 4;
  INVITE: 5;
  OFFER: 6;
  RINGING: 7;
}

export const CallEvent: CallEventMap;

export interface RespCodeMap {
  CONTINUE: 0;
  DROP: 1;
  RESPOND: 2;
  REPLACE: 3;
}

export const RespCode: RespCodeMap;

export interface CrudMap {
  CREATE: 0;
  UPDATE: 1;
  DELETE: 2;
}

export const Crud: CrudMap;

